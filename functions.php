<?php
/**
 * @package Dara Child Theme
 */

/**
* Load the parent and child theme styles
*/
add_action( 'wp_enqueue_scripts', 'dara_parent_theme_style',10 );

function dara_parent_theme_style() {

	// Parent theme styles
	wp_enqueue_style( 'dara-style', get_template_directory_uri(). '/style.css' );

	// Child theme styles
	wp_enqueue_style( 'dara-child-style', get_stylesheet_directory_uri(). '/style.css' );

}

add_filter('acf/settings/save_json', 'dara_child_json_save_point');

function dara_child_json_save_point( $path ) {

	// update path
	$path = get_stylesheet_directory() . '/assets/acf';


	// return
	return $path;

}

add_action( 'pre_get_posts', 'dara_child_get_testimonials' );

function dara_child_get_testimonials( $query )
{
	if (is_admin()){
		return;
	}

	if ( is_post_type_archive('jetpack-testimonial') ){
		$query->set( 'posts_per_page', 25 );
		$query->set( 'orderby', 'menu_order' );
		$query->set( 'order', 'ASC' );
	}
	return $query;
}

/**
 * Change post type labels and arguments for Testimonial Post Type plugin.
 *
 * @param array $args Existing arguments.
 *
 * @return array Amended arguments.
 */
add_action('init','dara_child_register_jetpack_testimonial_cpt');

function dara_child_register_jetpack_testimonial_cpt() {
	register_post_type( 'jetpack-testimonial', array(
		'description' => __( 'Customer Testimonials', 'jetpack' ),
		'labels' => array(
			'name'                  => esc_html__( 'Testimonials',                   'jetpack' ),
			'singular_name'         => esc_html__( 'Testimonial',                    'jetpack' ),
			'menu_name'             => esc_html__( 'Testimonials',                   'jetpack' ),
			'all_items'             => esc_html__( 'All Testimonials',               'jetpack' ),
			'add_new'               => esc_html__( 'Add New',                        'jetpack' ),
			'add_new_item'          => esc_html__( 'Add New Testimonial',            'jetpack' ),
			'edit_item'             => esc_html__( 'Edit Testimonial',               'jetpack' ),
			'new_item'              => esc_html__( 'New Testimonial',                'jetpack' ),
			'view_item'             => esc_html__( 'View Testimonial',               'jetpack' ),
			'search_items'          => esc_html__( 'Search Testimonials',            'jetpack' ),
			'not_found'             => esc_html__( 'No Testimonials found',          'jetpack' ),
			'not_found_in_trash'    => esc_html__( 'No Testimonials found in Trash', 'jetpack' ),
			'filter_items_list'     => esc_html__( 'Filter Testimonials list',       'jetpack' ),
			'items_list_navigation' => esc_html__( 'Testimonial list navigation',    'jetpack' ),
			'items_list'            => esc_html__( 'Testimonials list',              'jetpack' ),
		),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'page-attributes',
			'revisions',
		),
		'rewrite' => array(
			'slug'       => 'aanbevelingen',
			'with_front' => false,
			'feeds'      => false,
			'pages'      => true,
		),
		'public'          => true,
		'show_ui'         => true,
		'menu_position'   => 20, // below Pages
		'menu_icon'       => 'dashicons-testimonial',
		'capability_type' => 'page',
		'map_meta_cap'    => true,
		'has_archive'     => true,
		'query_var'       => 'testimonial',
		'show_in_rest'    => false,
	) );
}
