<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Dara
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'components/post/content', get_post_format() );

			the_post_navigation( array( 'prev_text' => '<span class="title">' . esc_html__( 'Vorige aanbeveling', 'dara' ) . '</span>%title', 'next_text' => '<span class="title">' . esc_html__( 'Volgende aanbeveling', 'dara' ) . '</span>%title' ) );

		endwhile; // End of the loop.
		?>

		</main>
	</div>
<?php
get_sidebar();
get_footer();
