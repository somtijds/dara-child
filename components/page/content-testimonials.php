<?php
/**
 * The template used for displaying testimonials in page.php and page-templates.
 *
 * @package Dara
 */
?>

<?php
    $testimonials = get_field( 'add_testimonial' );
    if ( $testimonials ) : ?>
	<aside class="testimonials">
        <ul>
        <?php foreach( $testimonials as $post ) : ?>
            <?php setup_postdata( $post ); ?>
            <li>
                <?php get_template_part( 'components/testimonials/content', 'testimonial' ); ?>
            </li>
            <?php wp_reset_postdata(); ?>
        <?php endforeach; ?>
        </ul>
	</aside><!-- testimonials -->
<?php endif; ?>
