<div class="site-info">
	<?php $privacy_page = get_option( 'wp_page_for_privacy_policy '); ?>
	<?php if ( ! empty( $privacy_page ) && 'publish' === get_post_status( $privacy_page ) ) : ?>
	<span class="link-to-privacy-statement">
		<a href="<?php echo esc_url( get_the_permalink( $privacy_page ) ); ?>" title="<?php echo esc_attr( get_the_title( $privacy_page ) ); ?>"><?php echo esc_html( get_the_title( $privacy_page ) ); ?></a>
	</span>
	<?php endif; ?>
</div><!-- .site-info -->